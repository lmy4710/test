#!/bin/bash
#!/bin/bash
#
# author: weizhifeng
# description: 
# 定时查看haproxy是否存在，如果不存在则启动haproxy，
# 如果启动失败，则停止keepalived
# 
status=$(ps aux | grep sbin/haproxy | grep -v grep | grep -v bash | wc -l)
if [ "${status}" = "0" ]; then
    /etc/init.d/haproxy start

    status2=$(ps aux | grep sbin/haproxy | grep -v grep | grep -v bash |wc -l)

    if [ "${status2}" = "0"  ]; then
            /etc/init.d/keepalived stop
    fi
fi
